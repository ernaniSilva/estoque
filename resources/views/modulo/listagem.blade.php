@extends('layout/principal')
@section('conteudo')

    {{empty($atualizado) ? '' : 'Modulo Atualizado com sucesso'}}
    <table class="table table-striped table-bordered table-hover">
        @foreach($modulos as $key => $c)
            <tr>
                <td>
                    {{$c->titulo}}
                </td>
                <td>
                    {{$c->descricao}}
                </td>
                <td>                    
                    {{$c->status == 1 ? 'Ativo' : 'Inativo'}} 
                </td>
                <td>
                    <a href="/modulos/editar/{{$c->id}}">
                        <span class="glyphicon glyphicon-search">Editar</span>
                    </a>
                </td>
                <td><a href="/modulos/remove/{{$c->id}}"><span class="glyphicon glyphicon-trash">Del</span></a></td>
            </tr>  
        @endforeach
    </table>    

@stop