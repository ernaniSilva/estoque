
@extends('layout.principal')
@section('conteudo')

	<h1>Editar Atividades</h1>

	<form id="formId">
	  <input type="hidden" name="_method" value="PUT" />
	  <div class="form-group">
	    <label>Título</label>
	    <input name="titulo" value="{{$detalhe->titulo}}" class="form-control"/>
	  </div>
	  <div class="form-group">
	    <label>Descrição</label>
	    <input name="descricao" value="{{$detalhe->descricao}}" class="form-control"/>
	  </div>
    <div class="form-group">
			<label>Modulo</label>
		<select name="md" class="form-control">
					@foreach($modulo as $m)
					<option value="{{$m->id}}" {{$detalhe->modulo->id == $m->id ? 'selected' : ''}}>{{$m->titulo}}</option>
					@endforeach
			</select>
	  </div>    
    <div class="form-group">
      <label>Status</label>
      <select name="select" class="form-control">
          <option value="1" {{$detalhe->status == 1 ? 'selected' : ''}}>Ativo</option> 
          <option value="0" {{$detalhe->status == 0 ? 'selected' : ''}}>inativo</option>
      </select>
    </div>	  
	  <button type="submit" class="btn btn-primary btn-block">Submit</button>
	</form>

<script>
$( document ).ready(function() {
    $("#formId").submit(function(e){
        //e.preventDefault();        
        var form = $(this);          
        console.log(form.serialize());

        //Salva produto
        $.ajax({
            url : "http://localhost:8000/atividades/atualizar/{{$detalhe->id}}",
            type: 'PUT',
            dataType : 'json',
            data: form.serialize(),
            success : function(result){
                $(this).html("Success!");
                
            },
            error : function(){
                $(this).html("Error!");
                
            }
        });
    });
});
</script>

@stop