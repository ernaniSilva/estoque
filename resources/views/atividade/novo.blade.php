@extends('layout.principal')
@section('conteudo')

	<h1>Nova Atividade</h1>

	<form action="/atividades/adiciona" method="post">
	  
	  <input type="hidden" name="_token" value="{{{ csrf_token() }}}" />
		
	  <div class="form-group">
	    <label>Título</label>
	    <input name="titulo" class="form-control"/>
	  </div>
	  <div class="form-group">
	    <label>Descrição</label>
	    <input name="descricao" class="form-control"/>
	  </div>
		<div class="form-group">
			<label>Modulo</label>
		<select name="md" class="form-control">
					@foreach($modulo as $m)
					<option value="{{$m->id}}">{{$m->titulo}}</option>
					@endforeach
			</select>
	  </div>
	  <div class="form-group">
        <label>Status</label>
	    <select name="select" class="form-control">
            <option value="1">Ativo</option> 
            <option value="0" selected>inativo</option>            
        </select>
	  </div>
	  <button type="submit" class="btn btn-primary btn-block">Submit</button>
	</form>

@stop