@extends('layout/principal')
@section('conteudo')

    {{empty($atualizado) ? '' : 'Modulo Atualizado com sucesso'}}
    <table class="table table-striped table-bordered table-hover">
    
        @foreach($atv as $key => $a)
            <tr>
                <td>
                    {{$a->titulo}}
                </td>
                <td>
                    {{$a->descricao}}
                </td>
                <td>
                    {{$a->modulo->titulo}}
                </td>
                <td>                    
                    {{$a->status == 1 ? 'Ativo' : 'Inativo'}} 
                </td>
                <td>
                    <a href="/atividades/editar/{{$a->id}}">
                        <span class="glyphicon glyphicon-search">Editar</span>
                    </a>
                </td>
                <td><a href="/atividades/remove/{{$a->id}}"><span class="glyphicon glyphicon-trash">Del</span></a></td>
            </tr>  
        @endforeach
    </table>

@stop