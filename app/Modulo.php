<?php namespace estoque;

use Illuminate\Database\Eloquent\Model;

class Modulo extends Model {

	//
	
	protected $fillable = array('titulo','descricao', 'status');

	public function atividade(){

        return $this->hasMany('estoque\Atividade');
    }

}
