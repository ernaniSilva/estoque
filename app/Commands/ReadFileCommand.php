<?php namespace estoque\Commands;

use estoque\Commands\Command;

use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Bus\SelfHandling;
use Illuminate\Contracts\Queue\ShouldBeQueued;
use Illuminate\Support\Facades\Input;
use estoque\Produto;

class ReadFileCommand extends Command implements SelfHandling, ShouldBeQueued {

	use InteractsWithQueue, SerializesModels;

	/**
	 * Create a new command instance.
	 *
	 * @return void
	 */
	public function __construct()
	{
		//
	}

	/**
	 * Execute the command.
	 *
	 * @return void
	 */
	public function handle()
	{
		//arquivo que será executado em background
		//file_put_contents(storage_path()."/x.txt", 'Gravar');


		/*\Excel::load(Input::file('file'),function($reader){
            $reader->each(function($sheet){
                foreach ($sheet->toArray() as $row) {
                    User::firstOrCreate($sheet->toArray());
                }
            });
        });*/

        \Excel::load('public/upload/prod.xlsx',function($reader){
            $reader->each(function($sheet){
                foreach ($sheet->toArray() as $row) {
                    Produto::firstOrCreate($sheet->toArray());
                }
            });
        });    

	}

}
