<?php

Route::get('/', function(){

	return "<h1>Laravel Teste</h1>";
});


Route::get('/modulos/list', 'ModulosController@lista');

Route::get('/modulos/novo', 'ModulosController@novo');

Route::post('/modulo/adiciona', 'ModulosController@adiciona');

Route::get('/modulos/editar/{id}', 'ModulosController@editar');

Route::put('/modulos/atualizar/{id}', 'ModulosController@atualiza');

Route::get('/modulos/remove/{id}', 'ModulosController@remove');

Route::get('/atividades/list', 'AtividadesController@lista');

Route::get('/atividades/novo', 'AtividadesController@novo');

Route::post('/atividades/adiciona', 'AtividadesController@adiciona');

Route::get('/atividades/editar/{id}', 'AtividadesController@editar');

Route::put('/atividades/atualizar/{id}', 'AtividadesController@atualiza');

Route::get('/atividades/remove/{id}', 'AtividadesController@remove');