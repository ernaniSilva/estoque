<?php namespace estoque\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Request;
use estoque\Commands\ReadFileCommand;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Support\Facades\Input;
use estoque\Modulo;
use estoque\Atividade;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use estoque\Produto;
use estoque\User;

class AtividadesController extends Controller {

    use DispatchesCommands;

    public function lista(){

    	$data = [];

    	$atv = Atividade::all();

    	$data['atv'] = $atv;

        return view('atividade.listagem',$data);
    }

    public function adiciona(){

        $params = Request::all();
        $mod = new Atividade($params);    
        $mod->save();

        //Produto::create(Request::all());

    	return redirect('/modulos/list')->withInput(Request::only('titulo'));
    }

    public function novo(){

        return view('atividade.novo')->with('modulo', Modulo::all());
    }

    public function editar($id){

        $data = [];

        $atv = Atividade::find($id);

        $data['detalhe'] = $atv;

        return view('atividade.editar', $data)->with('modulo', Modulo::all());
    }

    public function atualiza($id){

        $data = [];

        $data['atualizado'] = 'ok';

        $parametros = Request::all();
        $atualiza = Atividade::find($id);
        $atualiza->update($parametros);

        return redirect('/atividades/list', $data);

    }

    public function remove($id){

        $atv = Atividade::find($id);
        $atv->delete();

        return redirect('/atividades/list');
    }
    
}