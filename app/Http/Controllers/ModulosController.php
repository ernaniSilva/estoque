<?php namespace estoque\Http\Controllers;

use Illuminate\Support\Facades\DB;
use Request;
use estoque\Commands\ReadFileCommand;
use Illuminate\Foundation\Bus\DispatchesCommands;
use Illuminate\Support\Facades\Input;
use estoque\Modulo;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use estoque\Produto;
use estoque\User;

class ModulosController extends Controller {

    use DispatchesCommands;

    public function lista(){

    	$data = [];

    	$modulos = Modulo::all();

    	$data['modulos'] = $modulos;

        return view('modulo.listagem',$data);
    }

    public function adiciona(){

        $params = Request::all();
        $mod = new Modulo($params);    
        $mod->save();

        //Produto::create(Request::all());

    	return redirect('/modulos/list')->withInput(Request::only('titulo'));
    }

    public function novo(){

        return view('modulo.novo');
    }

    public function editar($id){

        $data = [];

        $mod = Modulo::find($id);

        $data['detalhe'] = $mod;

        return view('modulo.editar', $data);
    }

    public function atualiza($id){

        $data = [];

        $data['atualizado'] = 'ok';

        $parametros = Request::all();
        $atualiza = Modulo::find($id);
        $atualiza->update($parametros);

        return redirect('/modulos/list', $data);

    }

    public function remove($id){

        $mod = Modulo::find($id);
        $mod->delete();

        return redirect('/modulos/list');
    }
    
}