<?php namespace estoque;

use Illuminate\Database\Eloquent\Model;

class Atividade extends Model {

	//
    protected $fillable = array('modulo_id','titulo', 'descricao','status');
    
    public function modulo(){

        return $this->belongsTo('estoque\Modulo');
    }

}
